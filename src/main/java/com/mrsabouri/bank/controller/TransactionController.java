package com.mrsabouri.bank.controller;

import com.mrsabouri.bank.controller.model.CreateTransactionRequest;
import com.mrsabouri.bank.service.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TransactionController {
  private TransactionService transactionService;

  @Autowired
  public TransactionController(TransactionService transactionService) {
    this.transactionService = transactionService;
  }

  @PostMapping("/transaction")
  public Boolean createTransaction(@RequestBody CreateTransactionRequest request) {
    return transactionService.createTransaction(request.getRegion(), request.getTransactionId(), request.getLegs());
  }
}
