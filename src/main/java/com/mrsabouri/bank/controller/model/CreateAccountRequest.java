package com.mrsabouri.bank.controller.model;

import java.math.BigDecimal;
import java.util.UUID;

public class CreateAccountRequest {
  private String region;
  private UUID accountId;
  private BigDecimal balance;

  public String getRegion() {
    return region;
  }

  public UUID getAccountId() {
    return accountId;
  }

  public BigDecimal getBalance() {
    return balance;
  }
}
