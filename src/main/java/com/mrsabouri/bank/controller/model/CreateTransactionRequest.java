package com.mrsabouri.bank.controller.model;

import java.math.BigDecimal;
import java.util.Map;
import java.util.UUID;

public class CreateTransactionRequest {
  private String region;
  private UUID transactionId;
  private Map<UUID, BigDecimal> legs;

  public String getRegion() {
    return region;
  }

  public UUID getTransactionId() {
    return transactionId;
  }

  public Map<UUID, BigDecimal> getLegs() {
    return legs;
  }
}
