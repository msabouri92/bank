package com.mrsabouri.bank.controller;

import com.mrsabouri.bank.controller.model.CreateAccountRequest;
import com.mrsabouri.bank.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AccountController {
  private AccountService accountService;

  @Autowired
  public AccountController(AccountService accountService) {
    this.accountService = accountService;
  }

  @PostMapping("/account")
  public Boolean createAccount(@RequestBody CreateAccountRequest request) {
    return accountService.createAccount(request.getRegion(), request.getAccountId(), request.getBalance());
  }
}
