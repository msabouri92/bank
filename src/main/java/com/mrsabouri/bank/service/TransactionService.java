package com.mrsabouri.bank.service;

import java.math.BigDecimal;
import java.util.Map;
import java.util.UUID;

public interface TransactionService {
  boolean createTransaction(String region, UUID transactionId, Map<UUID, BigDecimal> legs);
}
