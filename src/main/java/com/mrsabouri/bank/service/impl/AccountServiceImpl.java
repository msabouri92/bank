package com.mrsabouri.bank.service.impl;

import com.mrsabouri.bank.exception.AlreadyExistsException;
import com.mrsabouri.bank.model.Account;
import com.mrsabouri.bank.repository.AccountRepository;
import com.mrsabouri.bank.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.UUID;

@Component
public class AccountServiceImpl implements AccountService {
  private AccountRepository accountRepository;

  @Autowired
  public AccountServiceImpl(AccountRepository accountRepository) {
    this.accountRepository = accountRepository;
  }

  @Override
  public boolean createAccount(String region, UUID accountId, BigDecimal balance) {
    Account account = new Account(region, accountId, balance);
    try {
      accountRepository.save(account);
      return true;

    } catch (AlreadyExistsException aee) {
      return false;
    }
  }
}
