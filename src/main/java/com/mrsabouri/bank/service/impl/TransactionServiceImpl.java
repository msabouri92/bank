package com.mrsabouri.bank.service.impl;

import com.mrsabouri.bank.model.Transaction;
import com.mrsabouri.bank.repository.AccountRepository;
import com.mrsabouri.bank.repository.TransactionRepository;
import com.mrsabouri.bank.service.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Map;
import java.util.UUID;

@Component
public class TransactionServiceImpl implements TransactionService {
  private AccountRepository accountRepository;
  private TransactionRepository transactionRepository;

  @Autowired
  public TransactionServiceImpl(AccountRepository accountRepository, TransactionRepository transactionRepository) {
    this.accountRepository = accountRepository;
    this.transactionRepository = transactionRepository;
  }

  @Override
  @Transactional(isolation = Isolation.REPEATABLE_READ)
  public boolean createTransaction(String region, UUID transactionId, Map<UUID, BigDecimal> legs) {
    for (Map.Entry<UUID, BigDecimal> entry : legs.entrySet()) {
      UUID accountId = entry.getKey();
      BigDecimal amount = entry.getValue();

      try {
        accountRepository.updateBalance(region, accountId, amount);

      } catch (IllegalArgumentException iaex) {
        return false;
      }
    }

    Transaction transaction = new Transaction(region, transactionId, legs);
    transactionRepository.save(transaction);

    return true;
  }
}
