package com.mrsabouri.bank.service;

import java.math.BigDecimal;
import java.util.UUID;

public interface AccountService {
  boolean createAccount(String region, UUID accountId, BigDecimal balance);
}
