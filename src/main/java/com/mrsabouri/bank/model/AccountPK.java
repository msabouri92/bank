package com.mrsabouri.bank.model;

import java.util.UUID;

public class AccountPK {
  private String region;
  private UUID accountId;

  private AccountPK(String region, UUID accountId) {
    this.region = region;
    this.accountId = accountId;
  }

  public static AccountPK of(String region, UUID accountId) {
    return new AccountPK(region, accountId);
  }

  public String getRegion() {
    return region;
  }

  public UUID getAccountId() {
    return accountId;
  }
}
