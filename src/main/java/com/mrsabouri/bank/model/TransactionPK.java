package com.mrsabouri.bank.model;

import java.util.UUID;

public class TransactionPK {
  private String region;
  private UUID transactionId;

  private TransactionPK(String region, UUID transactionId) {
    this.region = region;
    this.transactionId = transactionId;
  }

  public static TransactionPK of(String region, UUID transactionId) {
    return new TransactionPK(region, transactionId);
  }

  public String getRegion() {
    return region;
  }

  public UUID getTransactionId() {
    return transactionId;
  }
}
