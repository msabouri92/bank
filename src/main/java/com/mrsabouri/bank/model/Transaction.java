package com.mrsabouri.bank.model;

import java.math.BigDecimal;
import java.util.Map;
import java.util.UUID;

public class Transaction {
  private TransactionPK pk;
  private Map<UUID, BigDecimal> legs;
  private boolean reversed;

  public Transaction(TransactionPK pk, Map<UUID, BigDecimal> legs) {
    this.pk = pk;
    this.legs = legs;
  }

  public Transaction(String region, UUID transactionId, Map<UUID, BigDecimal> legs) {
    this.pk = TransactionPK.of(region, transactionId);
    this.legs = legs;
  }

  public TransactionPK getPk() {
    return pk;
  }

  public Map<UUID, BigDecimal> getLegs() {
    return legs;
  }

  public boolean isReversed() {
    return reversed;
  }
}
