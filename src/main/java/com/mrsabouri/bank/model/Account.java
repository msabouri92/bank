package com.mrsabouri.bank.model;

import java.math.BigDecimal;
import java.util.UUID;

public class Account {
  private AccountPK pk;
  private BigDecimal balance;

  public Account(AccountPK pk, BigDecimal balance) {
    this.pk = pk;
    this.balance = balance;
  }

  public Account(String region, UUID accountId, BigDecimal balance) {
    this.pk = AccountPK.of(region, accountId);
    this.balance = balance;
  }

  public AccountPK getPk() {
    return pk;
  }

  public BigDecimal getBalance() {
    return balance;
  }

  public void setBalance(BigDecimal balance) {
    this.balance = balance;
  }
}
