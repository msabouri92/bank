package com.mrsabouri.bank.exception;

public class AlreadyExistsException extends RuntimeException {
  public AlreadyExistsException(Throwable throwable) {
    super(throwable);
  }
}
