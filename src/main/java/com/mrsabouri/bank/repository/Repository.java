package com.mrsabouri.bank.repository;

public interface Repository<V, K> {
  void save(V item);
}
