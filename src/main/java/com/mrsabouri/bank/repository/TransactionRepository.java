package com.mrsabouri.bank.repository;

import com.mrsabouri.bank.model.Transaction;
import com.mrsabouri.bank.model.TransactionPK;

public interface TransactionRepository extends Repository<Transaction, TransactionPK> {
}
