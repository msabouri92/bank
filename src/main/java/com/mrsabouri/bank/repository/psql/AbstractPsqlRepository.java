package com.mrsabouri.bank.repository.psql;

import com.mrsabouri.bank.exception.AlreadyExistsException;
import com.mrsabouri.bank.repository.Repository;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;

import java.util.Map;

public abstract class AbstractPsqlRepository<V, K> implements Repository<V, K> {
  private JdbcTemplate jdbcTemplate;
  private String tableName;

  public AbstractPsqlRepository(JdbcTemplate jdbcTemplate, String tableName) {
    this.jdbcTemplate = jdbcTemplate;
    this.tableName = tableName;
  }

  @Override
  public void save(V item) {
    SimpleJdbcInsert simpleJdbcInsert = new SimpleJdbcInsert(jdbcTemplate).withTableName(tableName);
    Map<String, Object> parameters = convertToParameters(item);

    try {
      simpleJdbcInsert.execute(parameters);
    } catch (DuplicateKeyException dkex) {
      throw new AlreadyExistsException(dkex);
    }
  }

  abstract Map<String, Object> convertToParameters(V item);
}
