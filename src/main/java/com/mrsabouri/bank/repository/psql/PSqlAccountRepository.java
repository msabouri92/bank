package com.mrsabouri.bank.repository.psql;

import com.mrsabouri.bank.model.Account;
import com.mrsabouri.bank.model.AccountPK;
import com.mrsabouri.bank.repository.AccountRepository;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Map;
import java.util.UUID;

@Component
public class PSqlAccountRepository extends AbstractPsqlRepository<Account, AccountPK> implements AccountRepository {
  private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

  @Autowired
  public PSqlAccountRepository(JdbcTemplate jdbcTemplate, NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
    super(jdbcTemplate, "account");
    this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
  }

  @Override
  Map<String, Object> convertToParameters(Account item) {
    return Map.of(
        "region", item.getPk().getRegion(),
        "account_id", item.getPk().getAccountId(),
        "balance", item.getBalance()
    );
  }

  @Override
  public void updateBalance(String region, UUID accountId, BigDecimal amount) {
    int updatedCnt = namedParameterJdbcTemplate.update(
        "UPDATE account SET balance = balance + :amount WHERE region = :region AND account_id = :accountId AND balance >= :balance",
        Map.of("amount", amount, "region", region, "accountId", accountId, "balance", amount.negate())
    );

    Validate.isTrue(updatedCnt == 1);
  }
}
