package com.mrsabouri.bank.repository.psql;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mrsabouri.bank.model.Transaction;
import com.mrsabouri.bank.model.TransactionPK;
import com.mrsabouri.bank.repository.TransactionRepository;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class PSqlTransactionRepository extends AbstractPsqlRepository<Transaction, TransactionPK> implements TransactionRepository {
  private ObjectMapper objectMapper;

  public PSqlTransactionRepository(JdbcTemplate jdbcTemplate, ObjectMapper objectMapper) {
    super(jdbcTemplate, "transaction");
    this.objectMapper = objectMapper;
  }

  @Override
  Map<String, Object> convertToParameters(Transaction item) {
    String legs;
    try {
      legs = objectMapper.writeValueAsString(item.getLegs());
    } catch (JsonProcessingException e) {
      throw new RuntimeException(e);
    }

    return Map.of(
        "region", item.getPk().getRegion(),
        "transaction_id", item.getPk().getTransactionId(),
        "legs", legs,
        "reversed", false
    );
  }
}
