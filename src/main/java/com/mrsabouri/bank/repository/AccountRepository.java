package com.mrsabouri.bank.repository;

import com.mrsabouri.bank.model.Account;
import com.mrsabouri.bank.model.AccountPK;

import java.math.BigDecimal;
import java.util.UUID;

public interface AccountRepository extends Repository<Account, AccountPK> {
  void updateBalance(String region, UUID accountId, BigDecimal amount);
}
